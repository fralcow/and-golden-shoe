from flask import Flask, render_template

app = Flask(__name__)

products = [
           {'product_id': 1, 'name': 'Bison Shoes', 'price': 50, 'image': 'usama-akram-kP6knT7tjn4-unsplash.jpg'},
           {'product_id': 2, 'name': 'Nifty Running', 'price': 75, 'image':'luis-felipe-lins-LG88A2XgIXY-unsplash.jpg'},
           {'product_id': 3, 'name': 'Shoya', 'price': 60, 'image':'mohammad-metri-E-0ON3VGrBc-unsplash.jpg'},
           {'product_id': 4, 'name': 'Shojet', 'price': 120, 'image':'maksim-larin-NOpsC3nWTzY-unsplash.jpg'},
           {'product_id': 5, 'name': 'Shoella', 'price': 55, 'image':'joseph-barrientos-4qSb_FWhHKs-unsplash.jpg'},
           {'product_id': 6, 'name': 'Swish Shoes', 'price': 70, 'image':'andres-jasso-PqbL_mxmaUE-unsplash.jpg'},
           {'product_id': 7, 'name': 'Warrior Shoes', 'price': 60, 'image':'irene-kredenets-dwKiHoqqxk8-unsplash.jpg'},
           {'product_id': 8, 'name': 'Run Running', 'price': 75, 'image': 'paul-gaudriault-a-QH9MAAVNI-unsplash.jpg'},
           {'product_id': 9, 'name': 'Stamina Shoes', 'price': 90, 'image': 'revolt-164_6wVEHfI-unsplash.jpg'}]

@app.route('/')
def index():
    return render_template('index.html', products=products)

@app.route('/product/<int:product_id>')
def product(product_id: int):
    return render_template('product.html', product=products[product_id-1])

if __name__ == "__main__":
    # Only for debugging while developing
    app.run(host='0.0.0.0', debug=True, port=80)
