# AND golden shoe

# Using the app
1. `make build` to build docker image
2. `make run` to run the image
3. With your browser go to `localhost:8000`

# Running without make
1. From the project root run `docker build .` to build the image
2. `docker run --rm --name golden-shoe-container -p 8000:80 golden-shoe` to run on port 8000
