FROM tiangolo/uwsgi-nginx-flask:python3.10
RUN apt-get update && apt-get install -y nano
COPY ./app /app
WORKDIR /app
RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt
